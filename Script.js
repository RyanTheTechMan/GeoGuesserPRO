// ==UserScript==
// @name         GeoGuesser PRO
// @namespace    null
// @version      1
// @description  Free GeoGuesser PRO
// @author       RyanTheTechMan
// @match        https://www.geoguessr.com/world/play/
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    $(".no-pro").removeClass("no-pro").addClass("yes-pro");
})();